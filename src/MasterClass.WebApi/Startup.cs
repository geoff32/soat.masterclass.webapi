using MasterClass.Business.DependencyInjection.Extensions;
using MasterClass.Core.Options;
using MasterClass.Repository.DependencyInjection.Extensions;
using MasterClass.WebApi.Context;
using MasterClass.WebApi.DependencyInjection.Extensions;
using MasterClass.WebApi.Middlewares;
using MasterClass.WebApi.StartupExtensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MasterClass.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<DiagnosticOptions>(Configuration.GetSection("Diagnostic"));
            
            services.ConfigureMock(Configuration);

            services.AddRepository();
            services.AddBusiness();
            services.AddService();

            services.AddScoped<IApplicationRequestContext, ApplicationRequestContext>();
            services.AddControllers();

            services.AddMasterClassSwagger();
            services.AddMasterClassAuthentication(Configuration);
            services.AddMasterClassAuthorization();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseMiddleware<TrackMachineMiddleware>();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<TrackRequestContextMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseMasterClassSwaggerUI();
        }
    }
}
