# Docker, Tests et CI

## CI  

Nous allons ajouter une CI sur gitlab. Il faudra donc que vous disposiez d'un compte et que votre repository soit hébergé dessus  

### Compilation et exécution des tests  

Pour la CI gitlab il nous suffit de rajouter un fichier *.gitlab-ci.yml* à la racine de notre repository  
Nous y définissons notamment :

- stages : Correspond aux différentes étapes de notre CI
- Une description de nos stages : image docker à récupérer, scripts, variables, etc...

Ajouter le fichier
> echo. >.gitlab-ci.yml  
```yml
stages:
  - build
  - test

variables:
  project: "MasterClass.WebApi"

build:
  stage: build
  image: mcr.microsoft.com/dotnet/sdk:5.0
  variables:
    build_path: "src/$project"
  before_script:
    - "dotnet restore"
  script:
    - "cd $build_path"
    - "dotnet build"

test:
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:5.0
  variables:
    test_path: "test"
  before_script:
    - "dotnet restore"
  script:
    - "cd $test_path"
    - "dotnet test /p:CollectCoverage=true"
```

Notre pipeline comprends 2 jobs :  

- build : Build notre projet
- test : Lance les tests  

Si Les runners partagés sont occupés, vous pouvez utiliser votre propre runner en l'installant dans un docker. Vous pouvez récupérer votre *[Registration Token]* en allant dans *Settings* puis *CI/CD* et en developpant la région *Runners* :
> docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest  
docker exec -it gitlab-runner gitlab-runner register --non-interactive --executor "docker" --docker-image alpine:latest --url "https://gitlab.com/" --registration-token [Registration Token] --description "masterclass-runner" --tag-list "docker" --run-untagged --locked="true"  

Il existe un badge permettant de récupérer le statut de notre pipeline dans gitlab  
Vous pouvez le paramétrer en allant dans le menu *Settings --> General --> Badges* :  

- lien :  https://gitlab.com/%{project_path}/commits/%{default_branch}
- image: https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg  

[![pipeline status](https://gitlab.com/geoff32/soat.masterclass.webapi/badges/master/pipeline.svg)](https://gitlab.com/geoff32/soat.masterclass.webapi/commits/master)

Comme le code coverage est indiqué dans le rapport de notre CI nous pouvons le récupérer via une regex dans *Settings* -> *CI / CD* -> *General pipelines*   
```regex
\|\s+MasterClass\.WebApi\s+\|\s+(\d+\.\d+\%)
```

Et ainsi utiliser un badge :  

- lien :  https://gitlab.com/%{project_path}/commits/%{default_branch}
- image: https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg  

[![coverage report](https://gitlab.com/geoff32/soat.masterclass.webapi/badges/master/coverage.svg)](https://gitlab.com/geoff32/soat.masterclass.webapi/commits/master)

### Publier votre documentation sur gitlab pages  

Gitlab propose la publication de site static  
Il existe des outils pour convertir nos fichiers Markdown en un site Web. Nous allons utiliser mkdocs  

Nous allons donc rajouter un stage *pages* dans notre CI

```yml
stages:
  - build
  - test
  - pages

variables:
  project: "MasterClass.WebApi"

build:
  stage: build
  image: mcr.microsoft.com/dotnet/sdk:5.0
  variables:
    build_path: "src/$project"
  before_script:
    - "dotnet restore"
  script:
    - "cd $build_path"
    - "dotnet build"

test:
  stage: test
  image: mcr.microsoft.com/dotnet/sdk:5.0
  variables:
    test_path: "test"
  before_script:
    - "dotnet restore"
  script:
    - "cd $test_path"
    - "dotnet test /p:CollectCoverage=true"

pages:
  stage: pages
  image: python:alpine
  before_script:
    - pip install mkdocs
    - pip install mkdocs-material
  script:
  - cp README.md docs/index.md
  - cp lessons -r docs/lessons
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
    - master
    - tags
  when: manual
```

En utilisant *only*, nous précisons que cette étape ne sera exécuté que sur notre branche *master* ou lors de la création d'un tag  
Le `when: manual` nous permet de spécifier que nous lancerons ce stage manuellement car nous ne souhaitons pas forcément déployer notre doc à chaque commit sur la *master*.  
Pour que mkdocs fonctionne il faut que les fichiers markdown soient mis dans un répertoire docs. Afin de préserver l'rchitecture du projet nous effectuons la copie pendant la CI.  
la commande *pip install mkdocs* permet d'installer mkdocs  
la commande *pip install mkdocs-material* permet d'installer le thème Material  
la commande *mkdocs builds* permet de générer un dossier *site* à partir des markdowns présents dans le répertoire docs.  
Enfin pour que gitlab pages fonctionne il faut que le site soit dans un répertoire *public* que nous déployons à l'aide d'*artifacts*  

Enfin il nous faut un fichier *mkdocs.yml* afin de configurer le build mkdocs  
> echo . > mkdocs.yml
```yml
site_name: MasterClass WebApi .Net Core
site_url: https://geoff32.gitlab.io/soat.masterclass.webapi
site_description: 'Une MasterClass pour appréhender les projets WebApi .Net Core de bout en bout'
site_author: 'Geoffroy Herb'
theme:
  name: 'material'
  language: 'fr'
  favicon: 'images/favicon.ico'
  custom_dir: 'custom-theme'
  palette:
    primary: lime
    accent: lime
nav:
  - Partie 1 :
    - Sommaire : lessons/lesson1/README.md
    - Initialisation du projet : lessons/lesson1/Initialize.md
    - Action et routage : lessons/lesson1/Routing.md
    - Middleware : lessons/lesson1/Middleware.md
    - Injection de dépendances : lessons/lesson1/DependencyInjection.md
  - Partie 2 :
    - Sommaire : lessons/lesson2/README.md
    - Utilisation du fichier de settings : lessons/lesson2/Settings.md
    - Logging : lessons/lesson2/Logging.md
    - Layers : lessons/lesson2/Layers.md
    - Utilisation des layers : lessons/lesson2/Authentification.md
    - Swagger : lessons/lesson2/Swagger.md
  - Partie 3 :
    - Sommaire : lessons/lesson3/README.md
    - Json Web Token : lessons/lesson3/Jwt.md
    - Cookie d'authentification : lessons/lesson3/Cookie.md
    - Autorisation : lessons/lesson3/Authorization.md
markdown_extensions:
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true
```

Nous précisons dans ce fichier un favicon que nous avons positionné dans le répertoire docs/images/
Nous précisons également un répertoire custom_dir : cela nous permet de surcharger des éléments comme par exemple le footer  
> mkdir custom-theme  
mkdir custom-theme/partials  
echo . > custom-theme/partials/footer.html  

```html
<footer class="md-footer">
    <div class="md-footer-meta md-typeset">
        <div class="md-footer-meta__inner md-grid">
        <div class="md-footer-copyright">
            powered by
            <a href="https://gitlab.com/geoff32" target="_blank">Geoffroy Herb</a>
            -
            <a href="https://www.soat.fr/" target="_blank">SOAT</a>
        </div>
        </div>
    </div>
</footer>
```

### Publication d'images docker  

Nous allons créer un fichier dockerfile pour publier une image de build

> echo . > Dockerfile.build
```Dockerfile
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY src/MasterClass.WebApi/MasterClass.WebApi.csproj src/MasterClass.WebApi/
RUN dotnet restore src/MasterClass.WebApi/MasterClass.WebApi.csproj
COPY . .
WORKDIR /src/src/MasterClass.WebApi
RUN dotnet build MasterClass.WebApi.csproj -c Release -o /app
```

Et un fichier dockerfile pour publier notre application

> echo . > Dockerfile.publish
```Dockerfile
ARG BUILD_IMAGE

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM ${BUILD_IMAGE} AS publish
WORKDIR /src/src/MasterClass.WebApi
RUN dotnet publish MasterClass.WebApi.csproj -c Release -o /app

FROM base AS final

WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT dotnet MasterClass.WebApi.dll
```

Et nous modifions notre fichier *.gitlab-ci.yml* en conséquence  
Nous allons créer un stage publish  

- pour les branches autre que master une publication automatique d'une image ayant pour tag le nom de la branche  
- pour les tags git une publication manuel d'une image latest et d'une image ayant pour tag le tag du commit  

```yml
variables:
  DOCKER_DRIVER: overlay
  DOCKER_BUILD_IMAGE: $CI_REGISTRY_IMAGE/build:$CI_BUILD_REF_SLUG

stages:
  - build
  - test
  - publish
  - pages

build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker build -t $DOCKER_BUILD_IMAGE -f Dockerfile.build .
    - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - docker push $DOCKER_BUILD_IMAGE

test:
  stage: test
  image: $DOCKER_BUILD_IMAGE
  variables:
    test_path: "/src/test"
  script:
    - cd $test_path
    - dotnet test /p:CollectCoverage=true

publish:
  stage: publish
  image: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/branch:$CI_BUILD_REF_SLUG
  script:
    - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - docker build --build-arg BUILD_IMAGE=$DOCKER_BUILD_IMAGE -t $DOCKER_IMAGE -f Dockerfile.publish .
    - docker push $DOCKER_IMAGE
  only:
    - branches
  except:
    - tags
    - master

tag:
  stage: publish
  image: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_TAG_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  script:
    - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - docker build --build-arg BUILD_IMAGE=$DOCKER_BUILD_IMAGE -t $CI_REGISTRY_IMAGE -t $DOCKER_TAG_IMAGE -f Dockerfile.publish .
    - docker push $DOCKER_TAG_IMAGE
    - docker push $CI_REGISTRY_IMAGE
  only:
    - tags
  when: manual

pages:
  stage: pages
  image: python:alpine
  before_script:
    - pip install mkdocs
    - pip install mkdocs-material
  script:
  - cp README.md docs/index.md
  - cp lessons -r docs/lessons
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
    - master
    - tags
  when: manual
```

!!! note  
    Nous utilisons des variables prédéfinies dans gitlab mais il est également possible d'en définir dans gitlab à partir du menu settings, par exemple dans le cas ou on veuille se connecter a un autre registry que celui de gitlab et ne pas mettre en dur ses informations d'authentification.   
    Cela se fait à partir du menu settings
    ![](../images/settings-gitlab-ci.png)