# Docker, Tests et CI  

## Tests  

### Initialisation de la solution de test  

Création d'une solution de tests
> mkdir test  
cd test  
dotnet new sln -n MasterClass.WebApi.Test  

Ajout d'un projet de test dans le répertoire test  
> dotnet new xunit -n MasterClass.WebApi.Test  
dotnet sln add ./MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj  

Nous référençons le projet à tester dans le projet de test
> cd ..  
dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj reference ./src/MasterClass.WebApi/MasterClass.WebApi.csproj

Afin de bénéficier de l'intellisense de l'extension omnisharp nous le rajoutons dans notre fichier *MasterClass.WebApi.sln*
> dotnet sln add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj  

Ajout du package *Moq* qui nous permettra de mocker nos objets
> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Moq --version 4.10.0  

Et du package *Microsoft.AspNetCore.Mvc.ViewFeatures* pour utiliser nos controllers  
> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Microsoft.AspNetCore.Mvc.ViewFeatures --version 2.2.0  

Si vous avez installé l'extension *.NET Core Test Explorer* il faut rajouter cette config dans le fichier *settings.json* (répertoire *.vscode*)  
```json
"dotnet-test-explorer.testProjectPath": "test"
```

### Test unitaire  

Ajout d'un builder pour générer notre *Fixture* (fixe un environnement pour l'exécution des tests)
> mkdir ./test/MasterClass.WebApi.Test/Controllers  
mkdir ./test/MasterClass.WebApi.Test/Controllers/Fixtures  
echo . > ./test/MasterClass.WebApi.Test/Controllers/Fixtures/UserControllerFixtureBuilder.cs
```c#
using System;
using MasterClass.Service.Abstractions.Users;
using MasterClass.Service.Models.Users;
using MasterClass.WebApi.Controllers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace MasterClass.WebApi.Test.Controllers.Fixtures
{
    public class UserControllerFixtureBuilder
    {
        private ServiceCollection _services;
        private readonly Mock<IUserService> _mockUserService;

        public UserControllerFixtureBuilder()
        {
            _services = new ServiceCollection();
            _mockUserService = new Mock<IUserService>();
        }

        public UserControllerFixtureBuilder Initialize()
        {
            _services.Clear();
            
            _services.AddSingleton<IUserService>(_mockUserService.Object);
            _services.AddSingleton<ISystemClock, SystemClock>();
            _services.AddTransient<UserController>();

            return this;
        }

        public UserControllerFixtureBuilder AddValidAuthentication(AuthenticateParameters authParams, AuthenticatedUser user)
        {
            _mockUserService.Setup(userService => userService.Authenticate(authParams))
                .Returns(user);

            return this;
        }

        public UserControllerFixtureBuilder AddInvalidAuthentication(AuthenticateParameters authParams)
        {
            _mockUserService.Setup(userService => userService.Authenticate(authParams))
                .Returns((AuthenticatedUser)null);
                
            return this;
        }

        public IServiceProvider Build()
        {
            return _services.BuildServiceProvider();
        }
    }
}
```

Ajout d'une classe de test pour tester notre controller *UserController*
> echo . > ./test/MasterClass.WebApi.Test/Controllers/UserController.test.cs  
```c#
using MasterClass.Service.Models.Users;
using MasterClass.WebApi.Controllers;
using MasterClass.WebApi.Test.Controllers.Fixtures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class UserControllerTest : IClassFixture<UserControllerFixtureBuilder>
    {
        private readonly UserControllerFixtureBuilder _fixtureBuilder;

        public UserControllerTest(UserControllerFixtureBuilder fixtureBuilder)
        {
            _fixtureBuilder = fixtureBuilder;
        }

        #region Authenticate
        [Fact]
        public void Authenticate_Valid()
        {
            //Given
            var authParams = Mock.Of<AuthenticateParameters>();
            var authUser = Mock.Of<AuthenticatedUser>();

            var userController = _fixtureBuilder
                .Initialize()
                .AddValidAuthentication(authParams, authUser)
                .Build().GetService<UserController>();

            //When
            var actionResult = userController.Authenticate(authParams);

            //Then
            Assert.IsAssignableFrom<OkObjectResult>(actionResult);
            var model = (actionResult as OkObjectResult)?.Value;
            Assert.IsAssignableFrom<AuthenticatedUser>(model);
            Assert.NotNull(model);
            Assert.Equal(authUser, model);
        }

        [Fact]
        public void Authenticate_Invalid()
        {
            //Given
            var authParams = Mock.Of<AuthenticateParameters>();
            
            var userController = _fixtureBuilder
                .Initialize()
                .AddInvalidAuthentication(authParams)
                .Build().GetService<UserController>();

            //When
            var actionResult = userController.Authenticate(authParams);

            //Then
            Assert.IsAssignableFrom<UnauthorizedResult>(actionResult);
            Assert.NotNull(actionResult);
        }

        #endregion
    }
}
```

A ce stade vous avez une erreur car il est impossible de mocker une instance de *AuthenticatedUser* car le seul moyen de construire une nouvelle instance est une méthode static internal. Il y a l'option de facilité où on rajoute un constructeur public sans paramètre mais nous n'aurions pas eu cette problématique si nous utilisions une interface pour manipuler la donnée  
> mkdir ./src/MasterClass.Service/Abstractions/Models  
mkdir ./src/MasterClass.Service/Abstractions/Models/Users  
echo . > ./src/MasterClass.Service/Abstractions/Models/Users/IAuthenticatedUser.cs  
```c#
namespace MasterClass.Service.Abstractions.Models.Users
{
    public interface IAuthenticatedUser
    {
        int Id { get; }
        string Token { get; }
    }
}
```

On fait hériter notre classe *AuthenticatedUser* de cette interface  
```c#
public class AuthenticatedUser : IAuthenticatedUser
```

Et on remplace les utilisations de la classe par son abstractions :  
*IUserService*  
```c#
IAuthenticatedUser Authenticate(AuthenticateParameters authParams);
```
*UserService*  
```c#
public IAuthenticatedUser Authenticate(AuthenticateParameters authParams)
```
*UserControllerTest*
```c#
public void Authenticate_Valid()
{
    var authUser = Mock.Of<IAuthenticatedUser>();
    ...
    Assert.IsAssignableFrom<IAuthenticatedUser>(model);
}
```
*UserControllerFixtureBuilder*
```c#
public UserControllerFixtureBuilder AddValidAuthentication(AuthenticateParameters authParams, IAuthenticatedUser user)
...
public UserControllerFixtureBuilder AddInvalidAuthentication(AuthenticateParameters authParams)
{
    _mockUserService.Setup(userService => userService.Authenticate(authParams))
        .Returns((IAuthenticatedUser)null);
        
    return this;
}
```

Si vous n'avez pas installé l'extension *.NET Core Test Explorer* vous pouvez jouer les tests en utilisant l'intellisense de l'extension Omnisharp (actions possible sur les controlleurs et méthodes de test) ou en ligne de commande  
> dotnet test test  

### Code coverage

Il est possible de récupérer le taux de couverture de notre projet de test en installant un package nuget dans le projet  

> dotnet add .\test\MasterClass.WebApi.Test\MasterClass.WebApi.Test.csproj package coverlet.msbuild --version 2.3.0  

En lançant la commande suivante vous pourrez visualiser la couverture de test de votre projet  
> dotnet test test /p:CollectCoverage=true

Si vous utilisez l'extension *Coverage Gutters* vous pouvez générer un fichier *lcov.info* qui permettra de l'utiliser  
> dotnet test .\test\MasterClass.WebApi.Test\ /p:CollectCoverage=true /p:CoverletOutputFormat=lcov /p:CoverletOutput=./lcov.info  

En cliquant sur *Watch* sur la barre de pied de page de VS Code vous visualiserez dans votre classe quelles sont les lignes de code couvertes par un test  

Si vous voulez que ce fichier soit mis à jour à chaque sauvegarde d'un fichier vous pouvez utiliser la commande suivante  :
> dotnet watch --project .\test\MasterClass.WebApi.Test\ test /p:CollectCoverage=true /p:CoverletOutputFormat=lcov /p:CoverletOutput=./lcov.info

### Test d'intégration  

Le projet de test doit référencer les packages suivants :  

- Microsoft.AspNetCore.Mvc.Testing  

> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Microsoft.AspNetCore.Mvc.Testing --version 3.0.0  
dotnet remove ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Microsoft.AspNetCore.Mvc.ViewFeatures  

Spécifiez le Kit de développement Web dans le fichier projet

```xml
<Project Sdk="Microsoft.NET.Sdk.Web">
```

Ajout d'un settings dédié pour notre environnement de CI

> echo . > ./src/MasterClass.WebApi/appsettings.ci.json

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Error"
    }
  },
  "Diagnostic": {
    "HealthCheckContent": "system_ok"
  }
}
```

Ajout d'une classe de test afin de vérifier que notre test de vie répond sur la bonne route
> echo . > ./test/MasterClass.WebApi.Test/Controllers/DiagnosticController.test.cs  

```c#
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.WithWebHostBuilder(builder => builder.UseEnvironment("ci")).CreateClient();
        }

        [Fact]
        public async void HealthCheck_Get_Status200()
        {
            var response = await _client.GetAsync("api/_system/healthcheck");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
        }

        [Fact]
        public async void HealthCheck_Head_Status200()
        {
            var response = await _client.SendAsync(new HttpRequestMessage(HttpMethod.Head, "api/_system/healthcheck"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
```

Nous remarquons que dans ce cas nos 2 tests sont très proches.  
En utilisant l'attribut *Fact* il n'est pas possible de rendre la méthode de test paramétrable et de jouer le test autant de fois qu'il y a de jeux de données. Dans ce cas nous pouvons utiliser l'attribut *Theory*.  
Afin de spécifier le jeux de données, nous allons utiliser l'attribut *InlineData*. Notre classe devient donc  

```c#
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.WithWebHostBuilder(builder => builder.UseEnvironment("ci")).CreateClient();
        }

        [Theory]
        [InlineData("GET")]
        [InlineData("HEAD")]
        public async void HealthCheck_Get_Status200(string method)
        {
            var response = await _client.SendAsync(new HttpRequestMessage(new HttpMethod(method), "api/_system/healthcheck"));
            
            if (!HttpMethods.IsHead(method)
                && !HttpMethods.IsDelete(method)
                && !HttpMethods.IsTrace(method))
            {
                Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
            }

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
```

L'inconvénient de *InlineData* est que nous ne pouvons lui passer que des constantes. Dans notre cas nous aimerions prendre en paramètre les propriétés statiques *HttpMethod.Get* et *HttpMethod.Head*  
Nous pouvons alors utiliser l'attribut *ClassData* dans notre cas, en spécifiant un type. Ce type doit obligatoirement implémenter IEnumerable&lt;objet[]&gt;    

> mkdir ./test/MasterClass.WebApi.Test/Controllers/Data  
echo . > ./test/MasterClass.WebApi.Test/Controllers/Data/HealthCheckData.cs    

```c#
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;

namespace MasterClass.WebApi.Test.Controllers.Data
{
    public class HealthCheckData : IEnumerable<object[]>
    {
        private readonly List<object[]> _data;

        public HealthCheckData()
        {
            _data = new List<object[]>
            {
                new object[] { HttpMethod.Get },
                new object[] { HttpMethod.Head }
            };
        }

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
```

Et nous modifions notre class de test de la manière suivante  

```c#
using System.Net;
using System.Net.Http;
using MasterClass.WebApi.Test.Controllers.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.WithWebHostBuilder(builder => builder.UseEnvironment("ci")).CreateClient();
        }

        [Theory]
        [ClassData(typeof(HealthCheckData))]
        public async void HealthCheck_Get_Status200(HttpMethod method)
        {
            var response = await _client.SendAsync(new HttpRequestMessage(method, "api/_system/healthcheck"));
            
            if (!HttpMethods.IsHead(method.Method)
                && !HttpMethods.IsDelete(method.Method)
                && !HttpMethods.IsTrace(method.Method))
            {
                Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
            }

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
```