# Docker, Tests et CI  

## [Docker](Docker.md#docker) (35min)  
### [Génération d'un certificat](Docker.md#generation-dun-certificat)  
### [Environnement de production](Docker.md#environnement-de-production)  
### [Environnement de debug](Docker.md#environnement-de-debug)  

## [Tests](Test.md#tests) (25min)  
### [Initialisation de la solution de test](Test.md#initialisation-de-la-solution-de-test)  
### [Test unitaire](Test.md#test-unitaire)  
### [Code coverage](Test.md#code-coverage)  
### [Test d'intégration](Test.md#test-dintegration)  

## [CI](Ci.md#ci)  (40min)
### [Compilation et exécution des tests](Ci.md#compilation-et-exécution-des-tests)  
### [Publier votre documentation sur gitlab pages](Ci.md#publier-votre-documentation-sur-gitlab-pages)  
### [Publication d'images docker](Ci.md#publication-dimages-docker)  