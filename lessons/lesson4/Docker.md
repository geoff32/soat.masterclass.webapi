# Docker, Tests et CI  

## Docker  

La portabilité de .NET Core nous permet de faire tourner notre application sur un docker. Nous allons automatiser la création de conteneurs à la fois en debug mais aussi en production  

### Génération d'un certificat  

Afin de pouvoir faire tourner notre applicatif en https nous allons générer un certificat auto-signé. A partir du SDK 2.1 l'outil *dev-certs* est intégré :  
Si vous êtes sous windows
> dotnet dev-certs https -ep %USERPROFILE%\\.masterclass\https\masterclass.webapi.pfx -p soat  
dotnet dev-certs https --trust  

Si vous êtes sous linux ou macOS
> dotnet dev-certs https -ep ${HOME}/.masterclass/https/masterclass.webapi.pfx -p soat  
dotnet dev-certs https --trust

### Environnement de production  

Afin de profiter du formatage et de l'intellisense de l'extension docker vous pouvez rajouter un fichier de settings
> echo . > ./.vscode/settings.json

```json
{
    "files.associations": {
        "dockerfile.*": "dockerfile"
    }
}
```

Commençons par ajouter un fichier *.dockerignore* afin de ne pas prendre en compte certains fichiers dans docker  
> echo . > .dockerignore  
```txt
**/.classpath
**/.dockerignore
**/.env
**/.git
**/.gitignore
**/.project
**/.settings
**/.toolstarget
**/.vs
**/.vscode
**/*.*proj.user
**/*.dbmdl
**/*.jfm
**/azds.yaml
**/bin
**/charts
**/docker-compose*
**/Dockerfile*
**/node_modules
**/npm-debug.log
**/obj
**/secrets.dev.yaml
**/values.dev.yaml
README.md

```

Nous allons créer une configuration de déploiement pour l'environnement de production
> echo . > src/MasterClass.WebApi/Dockerfile

```Dockerfile
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY src/MasterClass.WebApi/MasterClass.WebApi.csproj src/MasterClass.WebApi/
RUN dotnet restore src/MasterClass.WebApi/MasterClass.WebApi.csproj
COPY . .
WORKDIR /src/src/MasterClass.WebApi
RUN dotnet build MasterClass.WebApi.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish MasterClass.WebApi.csproj -c Release -o /app

FROM base AS final

WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT dotnet MasterClass.WebApi.dll
```

!!! info "Que fait ce fichier ?"  
    - Il télécharge l'image mcr.microsoft.com/dotnet/aspnet:5.0 afin de faire tourner notre API
    - Il télécharge l'image mcr.microsoft.com/dotnet/sdk:5.0 afin de compiler notre projet
    - Il permet de préciser que notre conteneur écoutera les ports 80 et 443 au moment de son exécution
    - Il restore les dépendences de notre projet et le build en mode Release
    - Il publie le projet en mode Release
    - Il construit l'image final qui tourne sur le runtime mcr.microsoft.com/dotnet/aspnet:5.0 avec un dossier /app qui contient le résultat de la publication de notre projet

Nous utilisons des variables d'environnement afin de ne pas pousser dans git des données sensibles type le mot de passe de notre certificat  
Pour cela nous créeons un fichier *.env* à la racine (que nous ignorons dans GIT via le fichier *.gitignore*)  
> echo . > .env
```yml
CERT_PASSWORD=soat
CERT_PATH=.masterclass/https
CERT_FILENAME=masterclass.webapi.pfx
ASPNETCORE_Kestrel__Certificates__Default__Password=soat
ASPNETCORE_Kestrel__Certificates__Default__Path=/https/masterclass.webapi.pfx
```

Nous allons ensuite un fichier docker-compose
> echo . > docker-compose.yml
```yml
version: '3.4'

services:
  masterclasswebapi:
    image: masterclasswebapi
    build:
      context: .
      dockerfile: src/MasterClass.WebApi/Dockerfile
    ports:
      - 80:80
      - 443:443
    environment:
      - ASPNETCORE_ENVIRONMENT=Production
      - DOTNET_USE_POLLING_FILE_WATCHER=1
      - ASPNETCORE_URLS=https://+;http://+
      - ASPNETCORE_HTTPS_PORT=443
      - ASPNETCORE_Kestrel__Certificates__Default__Password=$CERT_PASSWORD
      - ASPNETCORE_Kestrel__Certificates__Default__Path=/https/$CERT_FILENAME
```
!!! info "Que fait ce fichier ?"  
    - Il build une image nommé *masterclass.webapi* à partir de notre fichier *Dockerfile*
    - Il publie un conteneur nommé *masterclass-webapi*
    - Les ports 80 et 443 de l'adresse IP 127.0.0.9 de notre hôte sont respectivement mappés sur les ports 80 et 443 de notre conteneur
    - Il définit des variables d'environnement (adresse du certificat, environnement de production, port https...)

Les volumes à définir étant différents par OS nous allons gérer une surcharge pour windows et macOS.  
Windows :  
> echo . > docker-compose.windows.yml  
```yml
version: '3.4'

services:
  masterclasswebapi:
    volumes:
      - $USERPROFILE/$CERT_PATH:/https
```

MacOS :  
> echo . > docker-compose.macOs.yml  
```yml
version: '3.4'

services:
  masterclasswebapi:
    volumes:
      - $HOME/$CERT_PATH:/https
```

!!! note  
    Les volumes permettent de partager des données entre nos conteneurs

Il est en effet possible de surcharger des fichiers avec docker-compose, nous allons donc l'exploiter.  
Le conteneur peut être monté en utilisant docker-compose
Windows :  
> docker-compose -f docker-compose.yml -f docker-compose.windows.yml up -d --build  

MacOS :  
> docker-compose -f docker-compose.yml -f docker-compose.macOs.yml up -d --build

Et détruit de la manière suivante.  
Windows :  
> docker-compose -f docker-compose.yml -f docker-compose.windows.yml down  

MacOS :  
> docker-compose -f docker-compose.yml -f docker-compose.macOs.yml down  

### Environnement de debug  

Ajout d'une tache par os dans le fichier *tasks.json* qui lancera le docker-compose

```json
{
    "type": "docker-build",
    "label": "docker-build: debug",
    "dependsOn": [
        "build"
    ],
    "dockerBuild": {
        "tag": "masterclass:dev",
        "target": "base",
        "dockerfile": "${workspaceFolder}/src/MasterClass.WebApi/Dockerfile",
        "context": "${workspaceFolder}",
        "pull": true
    },
    "netCore": {
        "appProject": "${workspaceFolder}/src/MasterClass.WebApi/MasterClass.WebApi.csproj"
    }
},
{
    "type": "docker-build",
    "label": "docker-build: release",
    "dependsOn": [
        "build"
    ],
    "dockerBuild": {
        "tag": "masterclass:latest",
        "dockerfile": "${workspaceFolder}/src/MasterClass.WebApi/Dockerfile",
        "context": "${workspaceFolder}",
        "pull": true
    },
    "netCore": {
        "appProject": "${workspaceFolder}/src/MasterClass.WebApi/MasterClass.WebApi.csproj"
    }
},
{
    "type": "docker-run",
    "label": "docker-run: debug",
    "dependsOn": [
        "docker-build: debug"
    ],
    "dockerRun": {
        "envFiles": [
            "${workspaceFolder}/.env"
        ],
        "volumes": [{
            "containerPath": "/https",
            "localPath": "$USERPROFILE/.masterclass/https"
        }]
    },
    "netCore": {
        "appProject": "${workspaceFolder}/src/MasterClass.WebApi/MasterClass.WebApi.csproj",
        "enableDebugging": true
    }
},
{
    "type": "docker-run",
    "label": "docker-run: osx-debug",
    "dependsOn": [
        "docker-build: debug"
    ],
    "dockerRun": {
        "envFiles": [
            "${workspaceFolder}/.env"
        ],
        "volumes": [{
            "containerPath": "/https",
            "localPath": "$HOME/.masterclass/https"
        }]
    },
    "netCore": {
        "appProject": "${workspaceFolder}/src/MasterClass.WebApi/MasterClass.WebApi.csproj",
        "enableDebugging": true
    }
},
{
    "type": "docker-run",
    "label": "docker-run: release",
    "dependsOn": [
        "docker-build: release"
    ],
    "dockerRun": {},
    "netCore": {
        "appProject": "${workspaceFolder}/src/MasterClass.WebApi/MasterClass.WebApi.csproj"
    }
}
```

Ajout d'une configuration dans le fichier *launch.json*

```json
{
    "name": "Docker .NET Core Launch",
    "type": "docker",
    "request": "launch",
    "preLaunchTask": "docker-run: debug",
    "osx": {
        "preLaunchTask": "docker-run: osx-debug"
    },
    "netCore": {
        "appProject": "${workspaceFolder}/src/MasterClass.WebApi/MasterClass.WebApi.csproj"
    }
}
```

Vous pouvez à présent débugguer la solution à la fois dans votre environnement mais aussi dans un docker