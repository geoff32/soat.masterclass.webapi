# Introduction  

## [Initialisation du projet](Initialize.md#initialisation-du-projet) (10min)
### [Prérequis](Initialize.md#prerequis)  
### [Création du projet](Initialize.md#creation-du-projet)  

## [Action et routage](Routing.md#action-et-routage) (15min)  
### [Table de routage](Routing.md#table-de-routage)  
### [Ajout d'une route](Routing.md#ajout-dune-route)  

## [Middleware](Middleware.md#middleware) (20min)  

## [Injection de dépendances](DependencyInjection.md#injection-de-dependances) (45min)  
### [Introduction](DependencyInjection.md#introduction)  
### [Injection de dépendance dans un middleware](DependencyInjection.md#injection-de-dependance-dans-un-middleware)  
### [Mieux comprendre les cycles de vie](DependencyInjection.md#mieux-comprendre-les-cycles-de-vie)  