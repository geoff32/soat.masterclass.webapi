# Introduction  

## Injection de dépendances  

### Introduction  

L’injection de dépendances est une technique de couplage faible entre des classes et leurs dépendances.  
La création d'une instance sera déportée de l'objet d'où elle sera utilisée. Il existe différents types de cycle de vie de ces instances :

- Transient : Instance créée à la demande
- Scoped : Instance créée à la requête
- Singleton : Instance créée à la première demande puis réutilisée sur toutes les autres  

### Injection de dépendance dans un middleware  

Afin de comprendre et illustrer ce principe important nous allons créer un nouveau middleware qui s'occupera de renseigner des informations de contexte de la requête dans le header de la réponse (en l'occurence un id lié à la requête pour l'exemple)

Création d'une interface pour le contexte de la requête
```c#
public interface IApplicationRequestContext
{
    Guid Id { get; }
}
```

Création de la classe qui implémente l'interface
```c#
public class ApplicationRequestContext : IApplicationRequestContext
{
    public ApplicationRequestContext()
    {
        Id = Guid.NewGuid();
    }
    public Guid Id { get; }
}
```

Création du middleware
```c#
public class TrackRequestContextMiddleware
{
    private readonly RequestDelegate _next;

    public TrackRequestContextMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context, IApplicationRequestContext requestContext)
    {
        context.Response.Headers.Add("X-Guid", requestContext.Id.ToString());
        await _next(context);
    }
}
```

Dans la classe *StartUp* nous ajoutons ce middleware
```c#
app.UseMiddleware<TrackRequestContextMiddleware>();
```

Et nous choisissons de résoudre notre interface sur un cycle de vie *Scope* de manière à ce que notre Guid soit crée à chaque requête
```c#
services.AddScoped<IApplicationRequestContext, ApplicationRequestContext>();
```

Nous voyons alors apparaitre un X-Guid dans le header de notre réponse qui change à chaque nouvelle requête.

### Mieux comprendre les cycles de vie  

Amusons nous avec l'injection de dépendance pour mieux comprendre les cycles de vie de nos objets. Commencez par faire un commit de vos modifs dans git (ou un autre outil de versionning) car il faudra faire un rollback à la fin de nos tests.  
Afin de vérifier que le périmètre scope est bien respecté, ajoutons notre *IApplicationRequestContext* dans le constructeur de notre controlleur et comparons les guid
```c#
[Route("api/_system")]
public class DiagnosticController : ControllerBase
{
    private readonly IApplicationRequestContext _requestContext;

    public DiagnosticController(IApplicationRequestContext requestContext) => _requestContext = requestContext;

    [HttpGet, HttpHead, Route("healthcheck")]
    public IActionResult HealthCheck()
    {
        ControllerContext.HttpContext.Response.Headers.Add("X-Guid2", _requestContext.Id.ToString());
        return Ok("system_ok");
    }
}
```

Pour aller plus loin changez l'enregistrement dans notre conteneur d'injection de dépendances sur un mode *Transient*
```c#
services.AddTransient<IApplicationRequestContext, ApplicationRequestContext>();
```
On constate alors que le Guid est différent entre notre controlleur et notre middleware

Changez de nouveau l'enregistrement pour revenir à un périmètre *Scope*.  
Maintenant nous allons changer notre middleware de manière à ce que la résolution se fasse au niveau du constructeur au lieu de la méthode
```c#
public class TrackRequestContextMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IApplicationRequestContext _requestContext;

    public TrackRequestContextMiddleware(RequestDelegate next, IApplicationRequestContext requestContext)
    {
        _next = next;
        _requestContext = requestContext;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        context.Response.Headers.Add("X-Guid", _requestContext.Id.ToString());
        await _next(context);
    }
}
```
La résolution de dépendance plante au démarrage de l'appli. Car l'instance du middleware est un singleton.  
```An unhandled exception of type 'System.InvalidOperationException' occurred in System.Private.CoreLib.dll: 'Cannot resolve scoped service 'MasterClass.WebApi.Context.IApplicationRequestContext' from root provider.'```  

Il faut donc changer le mode d'enregistrement de notre instance.  
```c#
services.AddSingleton<IApplicationRequestContext, ApplicationRequestContext>();
```

Nous constatons que le Guid est alors identique sur l'ensemble de nos requêtes...