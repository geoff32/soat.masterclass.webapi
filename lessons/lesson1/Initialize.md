# Introduction  

## Initialisation du projet  

### Prérequis  

* VS Code
* dotnet core sdk 5.0
* Docker et Docker Compose
* Omnisharp, Extensions C#, optionnelle Docker
* Fiddler ou Postman

### Création du projet  

Dans la console VS Code
Création d'un fichier solution
> dotnet new sln -n MasterClass.WebApi

Création d'un répertoire src
> mkdir src  
cd src

Création d'un projet webapi :
> dotnet new webapi -n MasterClass.WebApi

Ajout du projet dans la solution
> cd ..  
dotnet sln add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj

Build de la solution
> dotnet build

!!! note    
    Le plugin C# vous propose d'ajouter les fichiers nécessaires pour le debug  
    ![](../images/required_assets.png)