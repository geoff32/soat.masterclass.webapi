# Authentification et Autorisation  

## Autorisation  

### Ajouter des autorisations sur des actions  

Maintenant que nous avons un système d'authentification fonctionnel nous allons pouvoir aborder les autorisations.  
Les autorisations vont nous permettre de restreindre des actions (donc des routes de notre WebApi) en fonction de l'utilisateur connecté. Il est possible de définir ces restrictions à 3 niveaux :  

- au niveau de l'action : avec l'attribut Authorize. La restriction ne s'applique que sur l'action concernée  
- au niveau du controller : avec l'attribut Authorize. La restriction s'applique sur toutes les actions du controller (il est possible d'utiliser l'attribut *AllowAnonymous* pour lever cette restriction sur une action)
- de manière globale : avec le filtre *AuthorizeFilter* enregistré de la manière suivante dans la classe *Startup* (il est possible d'utiliser l'attribut *AllowAnonymous* pour lever cette restriction sur une action ou un controller)
```c#
services.AddMvc(options => options.Filters.Add(new AuthorizeFilter()))
```

Afin de pouvoir utiliser les autorisations, il nous faut définir un schéma pour le mode d'authentification *Challenge*  
Nous voulons utiliser ces restrictions pour notre schéma *Bearer* et *Cookies* nous allons donc le rajouter sur notre schéma virtuel dans notre méthode *AddMasterClassAuthentication()*  
```c#
services
    .AddAuthentication(options => options.DefaultAuthenticateScheme = options.DefaultChallengeScheme = DEFAULT_AUTHENTICATE_SCHEME)
```

Afin de le faire fonctionner dans le cas ou l'on désactive un des 2 schémas il faut également le définir au niveau de la déclaration de nos schémas.  
Méthode *AddMasterClassJwt()*  
```c#
services
    .AddAuthentication(options => options.DefaultAuthenticateScheme = options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme)
```
Méthode *AddMasterClassCookie()*  
```c#
services
    .AddAuthentication(options =>
        options.DefaultAuthenticateScheme
            = options.DefaultSignInScheme
            = options.DefaultSignOutScheme
            = options.DefaultChallengeScheme
            = CookieAuthenticationDefaults.AuthenticationScheme)
```

Ajoutons l'attribut *Authorize* au niveau de notre *UserController*. Et ajoutons l'attribut *AllowAnonymous* sur nos 2 méthodes d'authentification  
```c#
using System.Threading.Tasks;
using MasterClass.Service.Abstractions.Users;
using MasterClass.Service.Models.Users;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user"), Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [HttpGet]
        public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });

        [HttpPost, Route("authenticate"), AllowAnonymous]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }

        [HttpPost("signin"), AllowAnonymous]
        public async Task<IActionResult> SignInAsync([FromBody]AuthenticateParameters authParams)
        {
            var principal = _userService.SignIn(authParams, CookieAuthenticationDefaults.AuthenticationScheme);
            if (principal != null)
            {
                await HttpContext.SignInAsync(principal, new AuthenticationProperties { IsPersistent = true });
                return Ok();
            }
            return Unauthorized();
        }

        [HttpPost("signout")]
        public async Task<IActionResult> SignOutAsync()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }
    }
}
```

### Role  

Il est possible de restreindre l'accès à des actions ou des controlleurs suivants les rôles de l'utilisateur. Ajoutons par exemple cette restiction au niveau de l'action *GetContext* de notre *UserController*  
```c#
[HttpGet, Authorize(Roles = "SuperAdmin")]
public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });
```

### Policy

Nous pouvons regrouper un certain nombre de prérequis sous forme d'une *Policy*. Nous allons par exemple regrouper la gestion des roles au seins d'une *Policy*.  

Création de constantes pour centraliser le nom de nos *Policy*
> mkdir ./src/MasterClass.WebApi/Authorization  
echo . > ./src/MasterClass.WebApi/Authorization/Policies.cs  
```c#
namespace MasterClass.WebApi.Authorization.Policy
{
    public static class Policies
    {
        public const string REQUIRED_SUPERADMIN_ROLE = "RequiredSuperAdminRole";
        public const string REQUIRED_ADMIN_ROLE = "RequiredAdminRole";
        public const string REQUIRED_MINIMUM_AGE_ALCOHOL = "RequiredMinimumAgeAlcohol";
    }
}
```

Création d'un méthode d'extension pour enregistrer nos *Policy*  
> echo . > ./src/MasterClass.WebApi/DependencyInjection/Extensions/AuthorizationExtensions.cs  
```c#
using MasterClass.WebApi.Authorization.Policy;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.DependencyInjection.Extensions
{
    public static class AuthorizationExtensions
    {
        public static IServiceCollection AddMasterClassAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.REQUIRED_SUPERADMIN_ROLE, policy => policy.RequireRole("SuperAdmin"));
                options.AddPolicy(Policies.REQUIRED_ADMIN_ROLE, policy => policy.RequireRole("SuperAdmin", "Admin"));
            });
            return services;
        }
    }
}
```

Modifions l'attribut sur notre action *GetContext*
```c#
[HttpGet, Authorize(Policy = Policies.REQUIRED_SUPERADMIN_ROLE)]
public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });
```

Et ajoutons un appel à notre méthode d'extensiosn dans la classe *Startup*  
```c#
services.AddMasterClassAuthorization();
```

Nous allons faire évoluer la définition d'un *SuperAdmin*.  
Tel que cela a été fait jusque maintenant un *SuperAdmin* était un role à part entière.  
A partir de maintenant un *SuperAdmin* est un *Admin* avec un droit de *SuperAdmin*.

Ajout le nom de notre *Claim* sous forme de constante  
> echo . > ./src/MasterClass.Service/Identity/MasterClassClaims.cs  
```c#
using System.IO;

namespace MasterClass.Service.Identity
{
    public static class MasterClassClaims
    {
        public const string JWTROLE_CLAIMNAME = "role";
        public const string RIGHTS_CLAIMNAME = "rights";
    }
}
```

Nous ajoutons l'information *Rights* dans nos *Claims* dans notre classe *IdentityExtensions*
```c#
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using MasterClass.Repository.Models.Users;

namespace MasterClass.Service.Identity
{
    public static class IdentityExtensions
    {
        public static IEnumerable<Claim> GetJwtClaims(this User user, DateTimeOffset issuedAt)
        {
            foreach (var role in user.Roles)
            {
                yield return new Claim(MasterClassClaims.JWTROLE_CLAIMNAME, role);
            }

            foreach (var rightClaim in user.GetMasterClassClaims())
            {
                yield return rightClaim;
            }

            yield return new Claim(JwtRegisteredClaimNames.Iat, issuedAt.ToUnixTimeSeconds().ToString());
            yield return new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString());
            yield return new Claim(JwtRegisteredClaimNames.Sub, user.Login);
            yield return new Claim(JwtRegisteredClaimNames.GivenName, user.Name);
            yield return new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());
        }

        public static ClaimsPrincipal GetClaimsPrincipal(this User user, string scheme)
        {
            var identity = new GenericIdentity(user.Id.ToString(), scheme);
            identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Name));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Login));
            identity.AddClaims(user.GetMasterClassClaims());
            return new GenericPrincipal(identity, user.Roles);
        }

        private static IEnumerable<Claim> GetMasterClassClaims(this User user)
        {
            return user.Rights == null
                ? Enumerable.Empty<Claim>()
                : user.Rights.Select(right => new Claim(MasterClassClaims.RIGHTS_CLAIMNAME, right));
        }
    }
}
```

Maintenant nous allons faire évoluer notre Policy :
```c#
services.AddAuthorization(options =>
{
    options.AddPolicy(Policies.REQUIRED_SUPERADMIN_ROLE, policy =>
        policy.RequireRole("Admin")
            .RequireClaim(MasterClassClaims.RIGHTS_CLAIMNAME, "SuperAdmin"));
    options.AddPolicy(Policies.REQUIRED_ADMIN_ROLE, policy => policy.RequireRole("Admin"));
});
```

Modifions notre utilisateur dans le fichier *users.json*
```json
{
    "MockUsers": {
        "Users": [{
            "Id": 1,
            "Login": "login1",
            "Password": "pwd1",
            "Name": "name1",
            "BirthDate": "01/01/1990",
            "Roles": [
                "Admin",
                "User"
            ],
            "Rights": [
                "SuperAdmin"
            ]
        },
        {
            "Id": 2,
            "Login": "login2",
            "Password": "pwd2",
            "Name": "name2",
            "BirthDate": "01/01/1995",
            "Roles": [
                "Admin",
                "User"
            ],
            "Rights": [
            ]
        },
        {
            "Id": 3,
            "Login": "login3",
            "Password": "pwd3",
            "Name": "name3",
            "BirthDate": "01/01/2005",
            "Roles": [
                "User"
            ],
            "Rights": [
            ]
        }]
    }
}
```

### Requirement  

Nous pouvons avoir besoin de controle d'accès spécifique par exemple de controller qu'un utilisateur a plus de 18 ans pour pouvoir acheter de l'alcool  
Nous allons utiliser les Requirement dans ce but  
> mkdir ./src/MasterClass.WebApi/Authorization/Requirements  
echo . > ./src/MasterClass.WebApi/Authorization/Requirements/MinimumAgeRequirement.cs  
```c#
using Microsoft.AspNetCore.Authorization;

namespace MasterClass.WebApi.Authorization.Requirements
{
    public class MinimumAgeRequirement : IAuthorizationRequirement
    {
        public int MinimumAge { get; }

        public MinimumAgeRequirement(int minimumAge)
        {
            MinimumAge = minimumAge;
        }
    }
}
```

Nous ajoutons une Policy avec ce Requirement  
Classe *Policies*  
```c#
public const string REQUIRED_ALCOHOL_MAJORITY = "RequiredAlcoholMajority";
```

```c#
options.AddPolicy(Policies.REQUIRED_ALCOHOL_MAJORITY, policy =>
    policy.Requirements.Add(new MinimumAgeRequirement(18)));
```

Nous ajoutons une route qui utilise cette policy  
> echo . > ./src/MasterClass.WebApi/Controllers/AlcoholController.cs  
```c#
using MasterClass.WebApi.Authorization.Policy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/alcohol"), Authorize(Policy = Policies.REQUIRED_ALCOHOL_MAJORITY)]
    public class AlcoholController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }
    }
}
```

Nous ajoutons le *Claim* dans notre classe *IdentityExtensions*
```c#
private static IEnumerable<Claim> GetMasterClassClaims(this User user)
{
    var rightClaims = user.Rights == null
        ? Enumerable.Empty<Claim>()
        : user.Rights.Select(right => new Claim(MasterClassClaims.RIGHTS_CLAIMNAME, right));

    return rightClaims
        .Concat(new [] { new Claim(ClaimTypes.DateOfBirth, user.BirthDate.ToString() ) });
}
```

Nous rajoutons un handler qui va permettre de valider le Requirement  
> echo . > ./src/MasterClass.WebApi/Authorization/Requirements/MinimumAgeHandler.cs
```c#
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace MasterClass.WebApi.Authorization.Requirements
{
    public class MinimumAgeHandler : AuthorizationHandler<MinimumAgeRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                    MinimumAgeRequirement requirement)
        {
            if (!context.User.HasClaim(c => c.Type == ClaimTypes.DateOfBirth))
            {
                return Task.CompletedTask;
            }

            var dateOfBirth = Convert.ToDateTime(
                context.User.FindFirst(c => c.Type == ClaimTypes.DateOfBirth).Value);

            int calculatedAge = DateTime.Today.Year - dateOfBirth.Year;
            if (dateOfBirth > DateTime.Today.AddYears(-calculatedAge))
            {
                calculatedAge--;
            }

            if (calculatedAge >= requirement.MinimumAge)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
```

Et enfin nous enregistrons ce handler  dans notre méthode d'extension *AddMasterClassAuthorization()*
```c#
services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();
```