# Authentification et Autorisation  

## Json Web Token  

### Principe  

un token JWT se présente sous la forme {header}.{payload}.{signature}

- Le header est utilisé pour décrire le jeton
- le payload contient les informations
- la signature permet de s'assurer de la fiabilité de ces informations

Le header et le payload sont des objets json encodé en base64.
La signature est un hash de {header}.{payload} à partir d'une clé

Tout client ayant connaissance de la clé est donc capable de vérifier la validité de la signature. De plus le payload étant un json en base64, tout le monde est capable de récupérer son contenu  
La sécurité de cette solution vient du fait que si le header ou le payload sont modifiés la signature n'est plus valide.  
Il est également possible d'utiliser un algorithme de chiffrement asymétrique afin de fournir aux applicatifs une clé publique qui permet seulement de valider la signature et de garder secrète une clé privée qui permet de générer la signature.

L'authentification JWT a plusieurs avantages :

- La légéreté de la solution car aucun token n'est persisté  
- Un gain de performance car le client n'a pas besoin d'appeler le serveur pour récupérer les informations stockées dans le token (par contre attention à ne pas mettre de données sensibles dedans car tout le monde peut les lire)
- Une solution robuste

Par contre l'inconvénient majeure de cette solution est qu'il est impossible de révoquer un token avant la fin de sa validité sans perdre la légéreté de la solution

### Implémentation  

Nous allons partir sur un algorythme symétrique pour notre API car notre clé de chiffrement n'aura pas à être partagée (la génération et la validation du token seront faits systématiquement au niveau de notre API)

Ajout d'une classe d'option
> echo . > ./src/MasterClass.Core/Options/JwtOptions.cs
```c#
using System;

namespace MasterClass.Core.Options
{
    public class JwtOptions
    {
        public bool Enabled { get; set; }
        public string Issuer { get; set; }
        public string Key { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
```

et d'une entrée dans le *appSettings.json*
```json
"JwtOptions": {
  "Enabled": true,
  "Issuer": "soat.fr",
  "Key": "xa4Z9HqY%}s/v~F>",
  "Duration": "0.00:30:00"
}
```

On crée une méthode d'extension afin de convertir un *string* en *byte[]*
> mkdir ./src/MasterClass.Core/Tools  
echo . > ./src/MasterClass.Core/Tools/EncodingExtensions.cs
```c#
using System.Text;

namespace MasterClass.Core.Tools
{
    public static class EncodingExtensions
    {
        public static byte[] ToBytes(this string input)
        {
            return Encoding.UTF8.GetBytes(input);
        }
    }
}
```

On crée une méthode d'extension afin d'ajouter le schéma d'authentification et le middleware nécessaire pour valider l'authentification
echo . > ./src/MasterClass.WebApi/DependencyInjection/Extensions/AuthenticationExtensions.cs
```c#
using System;
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.WebApi.DependencyInjection.Extensions
{
    public static class AuthenticationExtensions
    {
        public static IServiceCollection AddMasterClassJwt(this IServiceCollection services, IConfiguration config)
        {
            var jwtConfigSection = config.GetSection(nameof(JwtOptions));
            services.Configure<JwtOptions>(jwtConfigSection);

            var jwtOptions = jwtConfigSection.Get<JwtOptions>();

            if (jwtOptions?.Enabled ?? false)
            {
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = jwtOptions.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                            AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                            ClockSkew = TimeSpan.FromSeconds(0)
                        };
                    });
            }

            return services;
        }
    }
}
```

On ajoute le package System.IdentityModel.Tokens.Jwt à notre couche de service afin de pouvoir y générer le token  
> dotnet add ./src/MasterClass.Service/MasterClass.Service.csproj package System.IdentityModel.Tokens.Jwt --version 5.2.4

On crée une méthode d'extension pour gérer la création de nos claims  
> mkdir ./src/MasterClass.Service/Identity  
echo . > ./src/MasterClass.Service/Identity/IdentityExtensions.cs  
```c#
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using MasterClass.Repository.Models.Users;

namespace MasterClass.Service.Identity
{
    public static class IdentityExtensions
    {
        private const string JWTROLE_CLAIMNAME = "role";

        public static IEnumerable<Claim> GetJwtClaims(this User user, DateTimeOffset issuedAt)
        {
            foreach (var role in user.Roles)
            {
                yield return new Claim(JWTROLE_CLAIMNAME, role);
            }

            yield return new Claim(JwtRegisteredClaimNames.Iat, issuedAt.ToUnixTimeSeconds().ToString());
            yield return new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString());
            yield return new Claim(JwtRegisteredClaimNames.Sub, user.Login);
            yield return new Claim(JwtRegisteredClaimNames.GivenName, user.Name);
            yield return new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());
        }
    }
}
```

On ajoute un token au modèle exposé en sortie du *authenticate*
```c#
using MasterClass.Repository.Models.Users;

namespace MasterClass.Service.Models.Users
{
    public class AuthenticatedUser
    {
        public int Id { get; }

        public string Token { get; }

        private AuthenticatedUser(int id, string token)
        {
            Id = id;
            Token = token;
        }

        internal static AuthenticatedUser Create(User user, string token)
            => user == null ? null : new AuthenticatedUser(user.Id, token);
    }
}
```

On ajoute la reference à notre projet *MasterClass.Core* pour avoir accès aux options dans la couche de service. Et on ajoute également le package *Microsoft.AspNetCore.Authentication* afin de pouvoir utiliser *ISystemClock* pour récupérer l'heure (ce qui donnera la possibilité de la mocker en cas de test)  
> dotnet add ./src/MasterClass.Service/MasterClass.Service.csproj reference ./src/MasterClass.Core/MasterClass.Core.csproj  
dotnet add ./src/MasterClass.Service/MasterClass.Service.csproj package Microsoft.AspNetCore.Authentication --version 2.2.0  

On génère notre token dans notre couche de service en s'appuyant sur notre option (passée dans le constructeur par injection de dépendance)  
```c#
using System.IdentityModel.Tokens.Jwt;
using MasterClass.Business.Abstractions.Users;
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using MasterClass.Service.Abstractions.Users;
using MasterClass.Service.Identity;
using MasterClass.Service.Models.Users;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.Service.Users
{
    public class UserService : IUserService
    {
        private readonly IUserBusiness _userBusiness;
        private readonly JwtOptions _jwtOptions;
        private readonly ISystemClock _clock;

        public UserService(IUserBusiness userBusiness, IOptions<JwtOptions> jwtOptions, ISystemClock clock)
        {
            _userBusiness = userBusiness;
            _jwtOptions = jwtOptions.Value;
            _clock = clock;
        }

        public AuthenticatedUser Authenticate(AuthenticateParameters authParams)
        {
            var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
            if (user != null)
            {
                var issuedAt = _clock.UtcNow;
                var jwtToken = _jwtOptions.Enabled
                    ? new JwtSecurityToken(
                        issuer: _jwtOptions.Issuer,
                        claims: user.GetJwtClaims(issuedAt),
                        expires: issuedAt.LocalDateTime.Add(_jwtOptions.Duration),
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_jwtOptions.Key.ToBytes()), SecurityAlgorithms.HmacSha256))
                    : null;

                return AuthenticatedUser.Create(user, jwtToken == null ? null : new JwtSecurityTokenHandler().WriteToken(jwtToken));
            }
            return null;
        }
    }
}
```

Dans la classe *Startup* on appelle notre méthode d'extensions  
```c#
services.AddMasterClassJwt(Configuration);
```
Et on positionne le middleware d'authentification. Tous les middleware déclarés avant ne pourront pas s'appuyer sur l'authentification mais ceux d'après auront l'information. Si l'on veut exploiter l'authentification dans nos controllers il faudra donc la placer avant *app.UseMvc()*.  
```c#
app.UseAuthentication();
```

Le middleware d'authentification permet d'alimenter le *User* de notre HttpContext si les données d'authentification sont valides. Nous pouvons ensuite l'utiliser dans notre controller directement via la propriété *User*.  
Nous ajoutons une route dans notre *UserController* qui exploitera les données de l'utilisateur  
```c#
[HttpGet]
public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });
```

!!!note
    Si vous voulez utiliser l'authentification dans un autre middleware, vous devrez positionner le middleware d'authentification avant ce dernier

### Header d'authentification dans Swagger  

Nous allons rajouter de l'authentification sur notre swagger afin de pouvoir appeler les méthodes authentifiées (au niveau de notre classe *SwaggerExtensions*)
```c#
services.AddSwaggerGen(genOptions =>
{
    genOptions.SwaggerDoc(SWAGGER_DOCNAME,
        new Info
        {
            Title = "MasterClass WebApi",
            Version = VERSION
        });
    genOptions.AddJwtBearerSecurity();
});
```
```c#
private static void AddJwtBearerSecurity(this SwaggerGenOptions options)
{
    options.AddSecurityDefinition(
        JwtBearerDefaults.AuthenticationScheme,
        new OpenApiSecurityScheme
        {
            Name = "Authorization",
            In = ParameterLocation.Header
        });
        options.AddSecurityRequirement(
            new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme 
                    { 
                        Reference = new OpenApiReference 
                        { 
                            Type = ReferenceType.SecurityScheme,
                            Id = JwtBearerDefaults.AuthenticationScheme
                        } 
                    },
                    new string[] {}
                }
            });
}
```