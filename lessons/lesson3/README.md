# Authentification et Autorisation

## [Json Web Token](Jwt.md#json-web-token) (30min)  
### [Principe](Jwt.md#principe)  
### [Implémentation](Jwt.md#implementation)  
### [Header d'authentification dans Swagger](Jwt.md#header-dauthentification-dans-swagger)  

## [Cookie d'authentification](Cookie.md#cookie-dauthentification) (30min)  
### [Implémentation](Cookie.md#implementation)  
### [Authentification multiple](Cookie.md#authentification-multiple)  

## [Autorisation](Authorization.md#autorisation) (40min)  
### [Ajouter des autorisations sur des actions](Authorization.md#ajouter-des-autorisations-sur-des-actions)  
### [Role](Authorization.md#role)  
### [Policy](Authorization.md#policy)  
### [Requirement](Authorization.md#requirement)  