# Authentification et Autorisation  

## Cookie d'authentification

### Implémentation  

Nous allons gérer un deuxième mode d'authentification dans notre application.

Dans un premier temps les claims ne seront pas forcément identique suivant notre mode d'authentification, nous allons donc créer une autre méthode d'extension permettant de générer les claims de l'authentification par cookie dans notre classe *IdentityExtensions*  
```c#
public static ClaimsPrincipal GetClaimsPrincipal(this User user, string scheme)
{
    var identity = new GenericIdentity(user.Id.ToString(), scheme);
    identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Name));
    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Login));
    return new GenericPrincipal(identity, user.Roles);
}
```

Ajout d'une méthode *SignIn* dans notre layer de service qui va permettre de renvoyer un *ClaimsPrincipal*  
interface *IUserService*
```c#
ClaimsPrincipal SignIn(AuthenticateParameters authParams, string scheme);
```

classe *UserService*
```c#
public ClaimsPrincipal SignIn(AuthenticateParameters authParams, string scheme)
{
    var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
    return user == null ? null : user.GetClaimsPrincipal(scheme);
}
```

Ajout d'une route *signin* dans *UserController* qui va permettre de créer le cookie et d'une route *signout* qui va permettre de le supprimer
```c#
[HttpPost("signin")]
public async Task<IActionResult> SignInAsync([FromBody]AuthenticateParameters authParams)
{
    var principal = _userService.SignIn(authParams, CookieAuthenticationDefaults.AuthenticationScheme);
    if (principal != null)
    {
        await HttpContext.SignInAsync(principal, new AuthenticationProperties { IsPersistent = true });
        return Ok();
    }
    return Unauthorized();
}

[HttpPost("signout")]
public async Task<IActionResult> SignOutAsync()
{
    await HttpContext.SignOutAsync();
    return Ok();
}
```

Création d'un classe de settings pour gérer nos cookies
> echo . > ./src/MasterClass.Core/Options/CookieOptions.cs
```c#
using System;

namespace MasterClass.Core.Options
{
    public class CookieOptions
    {
        public bool Enabled { get; set; }
        public string Issuer { get; set; }
        public string Name { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
```

Et configuration de ces settings dans le fichier *appsettings.json*
```json
"CookieOptions": {
  "Enabled": true,
  "Issuer": "localhost",
  "Name": "MasterClass.Cookie",
  "Duration": "0.00:30:00"
}
```

Modifions notre classe d'extensions de manière à n'avoir qu'une méthode publique pour l'ajout de notre système d'authentification.  
```c#
using System;
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using MasterClass.WebApi.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.WebApi.DependencyInjection.Extensions
{
    public static class AuthenticationExtensions
    {
        public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
        {
            services.AddMasterClassJwt(config);
            services.AddMasterClassCookie(config);

            return services;
        }

        private static IServiceCollection AddMasterClassJwt(this IServiceCollection services, IConfiguration config)
        {
            var jwtConfigSection = config.GetSection(nameof(JwtOptions));
            services.Configure<JwtOptions>(jwtConfigSection);

            var jwtOptions = jwtConfigSection.Get<JwtOptions>();

            if (jwtOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options => options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = jwtOptions.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                            AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                            ClockSkew = TimeSpan.FromSeconds(0)
                        };
                    });
            }

            return services;
        }

        private static IServiceCollection AddMasterClassCookie(this IServiceCollection services, IConfiguration config)
        {
            var cookieOptions = config.GetSection(nameof(CookieOptions)).Get<CookieOptions>();
            if (cookieOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options =>
                        options.DefaultAuthenticateScheme
                            = options.DefaultSignInScheme
                            = options.DefaultSignOutScheme
                            = CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.Cookie.Name = cookieOptions.Name;
                        options.Cookie.Domain = cookieOptions.Issuer;
                        options.EventsType = typeof(WebApiCookieAuthenticationEvents);
                        options.ExpireTimeSpan = cookieOptions.Duration;
                    });

                services.AddScoped<WebApiCookieAuthenticationEvents>();
            }

            return services;
        }
    }
}
```

Nous créons une classe *WebApiCookieAuthenticationEvents* afin de surcharger les comportements de redirection standards de l'authent par cookie.  
> mkdir ./src/MasterClass.WebApi/Authentication  
echo . > ./src/MasterClass.WebApi/Authentication/WebApiCookieAuthenticationEvents.cs  

```c#
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace MasterClass.WebApi.Authentication
{
    public class WebApiCookieAuthenticationEvents : CookieAuthenticationEvents
    {
        public override async Task RedirectToAccessDenied(RedirectContext<CookieAuthenticationOptions> context)
        {
            await ReturnStatus(context.Response, StatusCodes.Status403Forbidden);
        }

        public async override Task RedirectToLogin(RedirectContext<CookieAuthenticationOptions> context)
        {
            await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
        }

        public async override Task RedirectToLogout(RedirectContext<CookieAuthenticationOptions> context)
        {
            await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
        }

        public async override Task RedirectToReturnUrl(RedirectContext<CookieAuthenticationOptions> context)
        {
            await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
        }

        private async Task ReturnStatus(HttpResponse response, int status)
        {
            response.StatusCode = status;
            await Task.CompletedTask;
        }
    }
}
```

Enfin nous appelons *AddMasterClassAuthentication()* dans la classe de *Startup* à la place de *AddMasterClassJwt()*
```c#
services.AddMasterClassAuthentication(Configuration);
```

### Authentification multiple  

Nous venons de rajouter une authentification par cookie. Cependant vous aurez peut être remarqué que l'authentification JWT ne fonctionne plus. Le token est correctement généré par la méthode authenticate mais il est impossible de s'authentifier avec. Cela est du au fait qu'il n'y a qu'un seul schéma d'authentification par défaut (*DefaultAuthenticateScheme*) dans .NET Core.  
Il est possible de contourner le problème en rajoutant un schéma virtuel avec la méthode *AddPolicyScheme()*. Ce schéma sera le schéma par défaut pour *Authenticate* et redirigera sur le bon schéma via un selector sur le *HttpContext*. Dans notre cas si la requête possède un header *Authorization* nous utiliserons le schéma de ce header, sinon nous prendrons le schéma *Cookies*    
```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using MasterClass.WebApi.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.WebApi.DependencyInjection.Extensions
{
    public static class AuthenticationExtensions
    {
        private const string DEFAULT_AUTHENTICATE_SCHEME = "DefaultAuthenticate";

        public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
        {
            var authenticationSchemes = services.AddMasterClassSchemes(config).ToList();

            if (authenticationSchemes.Count > 1)
            {
                services
                    .AddAuthentication(options => options.DefaultAuthenticateScheme = DEFAULT_AUTHENTICATE_SCHEME)
                    .AddPolicyScheme(DEFAULT_AUTHENTICATE_SCHEME, DEFAULT_AUTHENTICATE_SCHEME,
                        options => options.ForwardDefaultSelector = context => GetAuthenticateScheme(context, authenticationSchemes));
            }

            return services;
        }

        private static string GetAuthenticateScheme(Microsoft.AspNetCore.Http.HttpContext context, IEnumerable<string> authenticationSchemes)
        {
            var scheme = AuthenticationHeaderValue.TryParse(context.Request.Headers["Authorization"], out var authHeader)
                ? GetSchemeName(authHeader.Scheme, authenticationSchemes) : null;

            return scheme ?? GetSchemeName(CookieAuthenticationDefaults.AuthenticationScheme, authenticationSchemes) ?? authenticationSchemes.LastOrDefault();
        }

        private static string GetSchemeName(string scheme, IEnumerable<string> authenticationSchemes)
        {
            return authenticationSchemes.FirstOrDefault(s => string.Compare(scheme, s, true) == 0);
        }

        private static IEnumerable<string> AddMasterClassSchemes(this IServiceCollection services, IConfiguration config)
        {
            var jwtScheme = services.AddMasterClassJwt(config);
            if (!string.IsNullOrEmpty(jwtScheme))
            {
                yield return jwtScheme;
            }

            var cookieScheme = services.AddMasterClassCookie(config);
            if (!string.IsNullOrEmpty(cookieScheme))
            {
                yield return cookieScheme;
            }
        }

        private static string AddMasterClassJwt(this IServiceCollection services, IConfiguration config)
        {
            var jwtConfigSection = config.GetSection(nameof(JwtOptions));
            services.Configure<JwtOptions>(jwtConfigSection);

            var jwtOptions = jwtConfigSection.Get<JwtOptions>();

            if (jwtOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options => options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = jwtOptions.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                            AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                            ClockSkew = TimeSpan.FromSeconds(0)
                        };
                    });

                    return JwtBearerDefaults.AuthenticationScheme;
            }

            return null;
        }

        private static string AddMasterClassCookie(this IServiceCollection services, IConfiguration config)
        {
            var cookieOptions = config.GetSection(nameof(CookieOptions)).Get<CookieOptions>();
            if (cookieOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options =>
                        options.DefaultAuthenticateScheme
                            = options.DefaultSignInScheme
                            = options.DefaultSignOutScheme
                            = CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.Cookie.Name = cookieOptions.Name;
                        options.Cookie.Domain = cookieOptions.Issuer;
                        options.EventsType = typeof(WebApiCookieAuthenticationEvents);
                        options.ExpireTimeSpan = cookieOptions.Duration;
                    });

                services.AddScoped<WebApiCookieAuthenticationEvents>();

                return CookieAuthenticationDefaults.AuthenticationScheme;
            }

            return null;
        }
    }
}
```