# Structure de l'application  

## Utilisation des layers  

Nous allons maintenant utiliser nos différents layers.  
Notre objectif est de créer une route qui va permettre de valider les informations d'authentification de l'utilisateur
> POST api/user/authenticate

Nous allons pour cela implémenter les différents layers de notre applicatif

### Repository  

Nous n'allons pas nous interfacer avec une base de données mais utiliser un mock d'une base d'utilisateur

Ajout d'un fichier de settings pour les mock
> mkdir ./src/MasterClass.WebApi/Mock  
echo . > ./src/MasterClass.WebApi/Mock/users.json

```json
{
    "MockUsers": {
        "Users": [{
            "Id": 1,
            "Login": "login1",
            "Password": "pwd1",
            "Name": "name1",
            "BirthDate": "01/01/1990",
            "Roles": [
                "SuperAdmin",
                "Admin",
                "User"
            ],
            "Rights": [
            ]
        },
        {
            "Id": 2,
            "Login": "login2",
            "Password": "pwd2",
            "Name": "name2",
            "BirthDate": "01/01/1995",
            "Roles": [
                "Admin",
                "User"
            ],
            "Rights": [
            ]
        },
        {
            "Id": 3,
            "Login": "login3",
            "Password": "pwd3",
            "Name": "name3",
            "BirthDate": "01/01/2005",
            "Roles": [
                "User"
            ],
            "Rights": [
            ]
        }]
    }
}  
```

Création du modèle User
> mkdir ./src/MasterClass.Repository/Models  
mkdir ./src/MasterClass.Repository/Models/Users  
echo . > ./src/MasterClass.Repository/Models/Users/User.cs  

```c#
using System;

namespace MasterClass.Repository.Models.Users
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string[] Roles { get; set; }
        public string[] Rights { get; set; }
    }
}
```

Création de l'object Mock

> mkdir ./src/MasterClass.Repository/Mock/Users  
echo . > ./src/MasterClass.Repository/Mock/Users/MockUsers.cs  

```c#
using System.Collections.Generic;
using MasterClass.Repository.Models.Users;

namespace MasterClass.Repository.Mock.Users
{
    public class MockUsers
    {
        public List<User> Users { get; set; }
    }
} 
```

Nous allons installer le package Microsoft.Extensions.Options dans le projet MasterClass.Repository afin d'utiliser la résolution de notre fichier de config

> dotnet add ./src/MasterClass.Repository/MasterClass.Repository.csproj package Microsoft.Extensions.Options --version 5.0.0  

Nous créons une interface pour notre repository, qui expose une méthode permettant de récupérer un utilisateur via son login

> mkdir ./src/MasterClass.Repository/Abstractions/Users  
echo . > ./src/MasterClass.Repository/Abstractions/Users/IUserRepository.cs  

```c#
using MasterClass.Repository.Models.Users;

namespace MasterClass.Repository.Abstractions.Users
{
    public interface IUserRepository
    {
        User GetUser(string login);
    }
}
```

Et une implémentation de cette interface qui se base sur notre mock
> echo . > ./src/MasterClass.Repository/Mock/Users/MockUserRepository.cs  

```c#
using MasterClass.Repository.Abstractions.Users;
using MasterClass.Repository.Models.Users;
using Microsoft.Extensions.Options;
using System.Linq;

namespace MasterClass.Repository.Mock.Users
{
    public class MockUserRepository : IUserRepository
    {
        private readonly MockUsers _mock;

        public MockUserRepository(IOptions<MockUsers> mock) => _mock = mock.Value;

        public User GetUser(string login) => _mock.Users.SingleOrDefault(user => user.Login == login);
    }
}
```

### Business  

Notre couche business va s'occuper d'exposer une méthode permettant de savoir si les informations d'authentification sont valides

> mkdir ./src/MasterClass.Business/Abstractions  
mkdir ./src/MasterClass.Business/Abstractions/Users  
echo . > ./src/MasterClass.Business/Abstractions/Users/IUserBusiness.cs  

```c#
using MasterClass.Repository.Models.Users;

namespace MasterClass.Business.Abstractions.Users
{
     public interface IUserBusiness
    {
        User AuthenticateUser(string login, string password);
    }
}
```

L'implémentation de notre interface va s'appuyer sur notre couche de repository
> mkdir ./src/MasterClass.Business/Users  
echo . > ./src/MasterClass.Business/Users/UserBusiness.cs  

```c#
using MasterClass.Business.Abstractions.Users;
using MasterClass.Repository.Abstractions.Users;
using MasterClass.Repository.Models.Users;

namespace MasterClass.Business.Users
{
    public class UserBusiness : IUserBusiness
    {
        private readonly IUserRepository _userRepository;

        public UserBusiness(IUserRepository userRepository) => _userRepository = userRepository;

        public User AuthenticateUser(string login, string password)
        {
            var user = _userRepository.GetUser(login);
            return user != null && user.Password == password ? user : null;
        }
    }
}
```

### Service  

Notre couche service va s'occuper de formatter le résultat de la couche business  

Création de notre modèle en sortie avec une static factory
> mkdir ./src/MasterClass.Service/Models  
mkdir ./src/MasterClass.Service/Models/Users  
echo . > ./src/MasterClass.Service/Models/Users/AuthenticatedUser.cs
```c#
using MasterClass.Repository.Models.Users;

namespace MasterClass.Service.Models.Users
{
    public class AuthenticatedUser
    {
        public int Id { get; }

        private AuthenticatedUser(int id)
        {
            Id = id;
        }

        internal static AuthenticatedUser Create(User user)
            => user == null ? null : new AuthenticatedUser(user.Id);
    }
}
```

Création de notre modèle en entrée
> echo . > ./src/MasterClass.Service/Models/Users/AuthenticateParameters.cs
```c#
namespace MasterClass.Service.Models.Users
{
    public class AuthenticateParameters
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
```

Notre interface de service
> mkdir  ./src/MasterClass.Service/Abstractions  
mkdir ./src/MasterClass.Service/Abstractions/Users  
echo . > ./src/MasterClass.Service/Abstractions/Users/IUserService.cs  
```c#
using MasterClass.Service.Models.Users;

namespace MasterClass.Service.Abstractions.Users
{
    public interface IUserService
    {
        AuthenticatedUser Authenticate(AuthenticateParameters authParams);
    }
}
```

L'implémentation de notre interface de service
> mkdir ./src/MasterClass.Service/Users  
echo . > ./src/MasterClass.Service/Users/UserService.cs  
```c#
using MasterClass.Business.Abstractions.Users;
using MasterClass.Service.Abstractions.Users;
using MasterClass.Service.Models.Users;

namespace MasterClass.Service.Users
{
    public class UserService : IUserService
    {
        private readonly IUserBusiness _userBusiness;

        public UserService(IUserBusiness userBusiness) => _userBusiness = userBusiness;

        public AuthenticatedUser Authenticate(AuthenticateParameters authParams)
        {
            return AuthenticatedUser.Create(_userBusiness.AuthenticateUser(authParams.Login, authParams.Password));
        }
    }
}
```

### WebApi  

Création de notre controller qui va se base sur notre couche de service
> echo . > ./src/MasterClass.WebApi/Controllers/UserController.cs  

```c#
using MasterClass.Service.Abstractions.Users;
using MasterClass.Service.Models.Users;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [HttpPost, Route("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }
    }
}
```


### Résolution par injection de dépendances  

Il ne nous reste plus qu'à enregistrer nos différentes couches dans notre conteneur d'injection de dépendances. Par souci d'organisation nous allons créer une méthode d'extension pour chacune de nos couches.

Création de méthodes d'extension pour enregister notre mock  
> mkdir ./src/MasterClass.WebApi/DependencyInjection  
mkdir ./src/MasterClass.WebApi/DependencyInjection/Extensions  
echo . > ./src/MasterClass.WebApi/DependencyInjection/Extensions/MockExtensions.cs

```c#
using MasterClass.Repository.Mock.Users;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.DependencyInjection.Extensions
{
    public static class MockExtensions
    {
        public static IHostBuilder UseMockFiles(this IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureAppConfiguration((builderContext, configBuilder) =>
            {
                configBuilder.AddJsonFile("Mock/users.json");
            });

            return hostBuilder;
        }

        public static IServiceCollection ConfigureMock(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<MockUsers>(config.GetSection(nameof(MockUsers)));

            return services;
        }
    }
}
```

Création d'une méthode d'extension pour enregister notre couche repository
> mkdir ./src/MasterClass.Repository/DependencyInjection  
mkdir ./src/MasterClass.Repository/DependencyInjection/Extensions  
echo . > ./src/MasterClass.Repository/DependencyInjection/Extensions/RepositoryExtensions.cs  

```c#
using MasterClass.Repository.Abstractions.Users;
using MasterClass.Repository.Mock.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.Repository.DependencyInjection.Extensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepository, MockUserRepository>();

            return services;
        }
    } 
}
```

Création d'une méthode d'extension pour enregister notre couche business
> mkdir ./src/MasterClass.Business/DependencyInjection  
mkdir ./src/MasterClass.Business/DependencyInjection/Extensions  
echo . > ./src/MasterClass.Business/DependencyInjection/Extensions/BusinessExtensions.cs  

```c#
using MasterClass.Business.Abstractions.Users;
using MasterClass.Business.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.Business.DependencyInjection.Extensions
{
    public static class BusinessExtensions
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services.AddSingleton<IUserBusiness, UserBusiness>();

            return services;
        }
    } 
}
```

Création d'une méthode d'extension pour enregister notre couche service
> mkdir ./src/MasterClass.Service/DependencyInjection  
mkdir ./src/MasterClass.Service/DependencyInjection/Extensions  
echo . > ./src/MasterClass.Service/DependencyInjection/Extensions/ServiceExtensions.cs  

```c#
using MasterClass.Service.Abstractions.Users;
using MasterClass.Service.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddService(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();

            return services;
        }
    } 
}
```

Dans la classe *Program*, appeler la méthode *UseMockFiles()*  

```c#
public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        })
        .UseMockFiles();
```

Dans la classe *Startup*
```c#
services.ConfigureMock(Configuration);

services.AddRepository();
services.AddBusiness();
services.AddService();
```