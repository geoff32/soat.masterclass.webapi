# Structure de l'application  
## Logging  

Nous allons maintenant voir comment fonctionne le système de logging dans .Net Core.  
Lorsque nous utilisons le *CreateDefaultBuilder* dans notre classe *Program* la configuration de logging par défaut qui est appliqué est la suivante
```c#
.ConfigureLogging((Action<WebHostBuilderContext, ILoggingBuilder>) ((hostingContext, logging) =>
{
    logging.AddConfiguration((IConfiguration) hostingContext.Configuration.GetSection("Logging"));
    logging.AddConsole();
    logging.AddDebug();
}));
```

Il est possible de rajouter un fournisseur de log en appelant la méthode d'extension *ConfigureLogging* sur notre *WebHostBuilder* et d'ajouter un singleton sur une classe qui implémente *ILoggerProvider*

Voici par exemple ce que fait la méthode d'extensions *AddConsole*
```c#
public static ILoggingBuilder AddConsole(this ILoggingBuilder builder)
{
    builder.Services.AddSingleton<ILoggerProvider, ConsoleLoggerProvider>();
    return builder;
}
```

!!!note
    C'est bien de le savoir mais dans les faits le plus pratique est d'écrire dans la sortie standard  
    Nous pouvons ensuite récupérer la sortie standard et par exemple la pousser dans un elasticsearch puis dans un kibana (je ne vais pas rentrer dans les détails, voici [un lien intéressant](https://logz.io/blog/docker-logging/) si vous voulez creuser le sujet)

Nous allons voir comment utiliser la fonctionnalité de logging. Cela fonctionne naturellement via l'injection de dépendance  
Si nous reprenons le middleware précédent, nous décidons d'écrire ces informations dans un log plutot que dans le header de la réponse
```c#
public class TrackRequestContextMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<TrackRequestContextMiddleware> _logger;

    public TrackRequestContextMiddleware(RequestDelegate next, ILogger<TrackRequestContextMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context, IApplicationRequestContext requestContext)
    {
        _logger.LogInformation($"X-Guid : {requestContext.Id}");
        await _next(context);
    }
}
```

!!!warning
    Si vous ne voyez rien dans votre console, vérifier la config des logs car il est possible de définir le niveau de logs minimum  
    ```json
        "LogLevel": {
            "Default": "Information"
        }
    ```

## Serilog  

### Principe  

Serilog utilise l'interface fourni par Microsoft, mais y ajoute des particularités.  
Le principe est proche d'une gestion d'évènements : un log est émis, et des "sinks" capturent les logs.  
Par exemple, il existe le Sink Console qui écrit dans la console, ou le Sink File pour écrire dans un fichier. Il en existe plein : [Provided Sinks](https://github.com/serilog/serilog/wiki/Provided-Sinks). Il est également possible de faire le sien.

### Mise en place  

Deux façons de faire :  
En instanciant soi-même un ILoggerConfiguration.  

```c#
var log = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.File("log.txt")
    .CreateLogger();

log.Information("Hello, Serilog!");
```

Ou Serilog peut lire directement depuis le fichier de configuration JSON de l'application pour tout régler automatiquement :  

#### Contenu de Program.cs  

```c#
public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            })
            .UseSerilog(configureLogger: (hostContext, loggerConfig) => loggerConfig.ReadFrom.Configuration(hostContext.Configuration));
}
```

#### Contenu de appsettings.json  

```json
{
  "Serilog": {
    "Using":  ["Serilog.Sinks.Console"],
    "MinimumLevel": {
        "Default": "Information",
        "Override": {
            "Microsoft": "Warning",
            "System": "Warning",
            "My.Custom.Namespace": "Information"
        }
    }
    "WriteTo": [
      { "Name": "Console" },
      { "Name": "File", "Args": { "path": "%TEMP%\\Logs\\serilog-configuration-sample.txt" } }
    ],
    "Enrich": ["FromLogContext", "WithMachineName", "WithThreadId"],
    "Properties": {
        "Application": "Sample"
    }
  }
}
```

### Plus d'informations  

[Github](https://github.com/serilog/serilog)  
[Wiki - Configuration](https://github.com/serilog/serilog/wiki/Configuration-Basics)  
