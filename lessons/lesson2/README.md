# Structure de l'application

## [Utilisation du fichier de settings](Settings.md#utilisation-du-fichier-de-settings) (20min)  
### [Settings par défaut](Settings.md#settings-par-defaut)  
### [Settings par environnement](Settings.md#settings-par-environnement)

## [Logging](Logging.md#logging) (10min)

## [Layers](Layers.md#layers) (15min)
### [Repository](Layers.md#repository)
### [Business](Layers.md#business)
### [Service](Layers.md#service)
### [Interdépendances](Layers.md#interdependances)

## [Utilisation des layers](Authentification.md#utilisation-des-layers) (45min)
### [Repository](Authentification.md#repository)
### [Business](Authentification.md#business)
### [Service](Authentification.md#service)
### [WebApi](Authentification.md#webapi)
### [Résolution par injection de dépendances](Authentification.md#resolution-par-injection-de-dependances)

## [Swagger](Swagger.md#swagger) (15min)
### [Introduction](Swagger.md#introduction)
### [Documentation](Swagger.md#documentation)