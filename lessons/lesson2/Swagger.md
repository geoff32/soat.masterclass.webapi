
# Structure de l'application  

## Swagger  

### Introduction   

Swagger va nous permettre d'exposer une documentation de notre API. Il nous permettra également de lancer des appels sur notre API.  

### Documentation  

Nous allons ajouter le package *Swashbuckle.AspNetCore* à notre WebApi
> dotnet add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj package Swashbuckle.AspNetCore --version 5.6.3

On crée une méthode d'extension qui va permettre de générer un document décrivant notre API et d'enregistrer notre configuration
> echo . > ./src/MasterClass.WebApi/DependencyInjection/Extensions/SwaggerExtensions.cs
```c#
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace MasterClass.WebApi.DependencyInjection.Extensions
{
    public static class SwaggerExtensions
    {
        private const string VERSION = "v1";
        private const string SWAGGER_DOCNAME = "masterclass.webapi." + VERSION;

        public static IServiceCollection AddMasterClassSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(genOptions
                => genOptions.SwaggerDoc(SWAGGER_DOCNAME,
                    new OpenApiInfo
                    {
                        Title = "MasterClass WebApi",
                        Version = VERSION
                    }));

            return services;
        }
    }
}
```

On ajoute une autre méthode d'extension qui va permettre d'utiliser l'UI de swagger.  
*UseSwagger* permet d'exposer le doc généré ci dessus sous forme d'un json et *UseSwaggerUI* permet d'utiliser l'UI swagger
```c#
public static IApplicationBuilder UseMasterClassSwaggerUI(this IApplicationBuilder app)
{
    app.UseSwagger();
    app.UseSwaggerUI(uiOptions => uiOptions.SwaggerEndpoint($"/swagger/{SWAGGER_DOCNAME}/swagger.json", "MasterClass WebApi"));

    return app;
}
```

Dans la méthode *ConfigureServices* de la classe StartUp
```c#
services.AddMasterClassSwagger();
```

Dans la méthode *Configure* de la class *StartUp*
```c#
app.UseMasterClassSwaggerUI();
```

Vous pouvez accéder à l'interface de swagger à partir de l'url suivante:  
https://localhost:5001/swagger

### Sécurité
Si votre API utilise un serveur OAuth pour s'authentifier, il est possible de fournir à Swagger la configuration d'authentification pour pouvoir de lui-même aller chercher un jeton pour tester vos routes.
On doit alors ajouter dans la méthode *ConfigureServices* de la classe StartUp
```c#
app.UseSwaggerUI(uiOptions => 
	// (...)
	uiOptions.OAuthClientId(oAuthClientId);
	uiOptions.OAuthClientSecret(oAuthAppClientSecret);
	uiOptions.OAuthAppName(oAuthAppName);
	// (...)
);
```

Ainsi que dans la méthode *Configure* de la class *StartUp*
```c#
 services.AddSwaggerGen(genOptions => {
	// (...)
	SecurityScheme securityScheme = new OAuth2Scheme
	{
		Flow = "implicit",
		AuthorizationUrl = $"{authorityServerURL}/connect/authorize",
		Scopes = new Dictionary<string, string> { { "Scope1Name", "Scope1Description" }, { "Scope2Name", "Scope2Description" } }
	};
	genOptions.AddSecurityDefinition("oauth2", securityScheme);
	// (...)
});
```

#### Description
Vous pouvez dire à Swagger de rajouter une description comme quoi toutes vos routes peuvent retourner **401 : Unauthorized** ou **403 : Forbidden** en plus des status code définis par celles-ci.
```c#
public class AuthorizeCheckOperationFilter : IOperationFilter
{
	public IList<IDictionary<string, IEnumerable<string>>> Security => new List<IDictionary<string, IEnumerable<string>>> {
new Dictionary<string, IEnumerable<string>> { { "oauth2", new[] { APPLICATION_NAME } } } };
	
	public void Apply(Operation operation, OperationFilterContext context)
	{
	operation.Responses.Add("401", new Response { Description = "Unauthorized" });
	operation.Responses.Add("403", new Response { Description = "Forbidden" });
	operation.Security = this.Security;
	}
}
```