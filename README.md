# MasterClass WebApi .NET Core   

[![pipeline status](https://gitlab.com/geoff32/soat.masterclass.webapi/badges/master/pipeline.svg)](https://gitlab.com/geoff32/soat.masterclass.webapi/commits/master) [![coverage report](https://gitlab.com/geoff32/soat.masterclass.webapi/badges/master/coverage.svg)](https://gitlab.com/geoff32/soat.masterclass.webapi/commits/master)  

## Introduction  

### [Initialisation du projet](lessons/lesson1/Initialize.md#initialisation-du-projet) (10min)
#### [Prérequis](lessons/lesson1/Initialize.md#prerequis)  
#### [Création du projet](lessons/lesson1/Initialize.md#creation-du-projet)  

### [Action et routage](lessons/lesson1/Routing.md#action-et-routage) (15min)  
#### [Table de routage](lessons/lesson1/Routing.md#table-de-routage)  
#### [Ajout d'une route](lessons/lesson1/Routing.md#ajout-dune-route)  

### [Middleware](lessons/lesson1/Middleware.md#middleware) (20min)  

### [Injection de dépendances](lessons/lesson1/DependencyInjection.md#injection-de-dependances) (45min)  
#### [Introduction](lessons/lesson1/DependencyInjection.md#introduction)  
#### [Injection de dépendance dans un middleware](lessons/lesson1/DependencyInjection.md#injection-de-dependance-dans-un-middleware)  
#### [Mieux comprendre les cycles de vie](lessons/lesson1/DependencyInjection.md#mieux-comprendre-les-cycles-de-vie)  

## Structure de l'application

### [Utilisation du fichier de settings](lessons/lesson2/Settings.md#utilisation-du-fichier-de-settings) (20min)  
#### [Settings par défaut](lessons/lesson2/Settings.md#settings-par-defaut)  
#### [Settings par environnement](lessons/lesson2/Settings.md#settings-par-environnement)

### [Logging](lessons/lesson2/Logging.md#logging) (10min)

### [Layers](lessons/lesson2/Layers.md#layers) (15min)
#### [Repository](lessons/lesson2/Layers.md#repository)
#### [Business](lessons/lesson2/Layers.md#business)
#### [Service](lessons/lesson2/Layers.md#service)
#### [Interdépendances](lessons/lesson2/Layers.md#interdependances)

### [Utilisation des layers](lessons/lesson2/Authentification.md#utilisation-des-layers) (45min)
#### [Repository](lessons/lesson2/Authentification.md#repository)
#### [Business](lessons/lesson2/Authentification.md#business)
#### [Service](lessons/lesson2/Authentification.md#service)
#### [WebApi](lessons/lesson2/Authentification.md#webapi)
#### [Résolution par injection de dépendances](lessons/lesson2/Authentification.md#resolution-par-injection-de-dependances)

### [Swagger](lessons/lesson2/Swagger.md#swagger) (15min)
#### [Introduction](lessons/lesson2/Swagger.md#introduction)
#### [Documentation](lessons/lesson2/Swagger.md#documentation)

## Authentification et Autorisation

### [Json Web Token](lessons/lesson3/Jwt.md#json-web-token) (30min)  
#### [Principe](lessons/lesson3/Jwt.md#principe)  
#### [Implémentation](lessons/lesson3/Jwt.md#implementation)  
#### [Header d'authentification dans Swagger](lessons/lesson3/Jwt.md#header-dauthentification-dans-swagger)  

### [Cookie d'authentification](lessons/lesson3/Cookie.md#cookie-dauthentification) (30min)  
#### [Implémentation](lessons/lesson3/Cookie.md#implementation)  
#### [Authentification multiple](lessons/lesson3/Cookie.md#authentification-multiple)  

### [Autorisation](lessons/lesson3/Authorization.md#autorisation) (40min)  
#### [Ajouter des autorisations sur des actions](lessons/lesson3/Authorization.md#ajouter-des-autorisations-sur-des-actions)  
#### [Role](lessons/lesson3/Authorization.md#role)  
#### [Policy](lessons/lesson3/Authorization.md#policy)  
#### [Requirement](lessons/lesson3/Authorization.md#requirement)  

## Docker, Tests et CI  

### [Docker](lessons/lesson4/Docker.md#docker) (35min)  
#### [Génération d'un certificat](lessons/lesson4/Docker.md#generation-dun-certificat)  
#### [Environnement de production](lessons/lesson4/Docker.md#environnement-de-production)  
#### [Environnement de debug](lessons/lesson4/Docker.md#environnement-de-debug)  

### [Tests](lessons/lesson4/Test.md#tests) (25min)  
#### [Initialisation de la solution de test](lessons/lesson4/Test.md#initialisation-de-la-solution-de-test)  
#### [Test unitaire](lessons/lesson4/Test.md#test-unitaire)  
#### [Code coverage](lessons/lesson4/Test.md#code-coverage)  
#### [Test d'intégration](lessons/lesson4/Test.md#test-dintegration)  

### [CI](lessons/lesson4/Ci.md#ci)  (40min)
#### [Compilation et exécution des tests](lessons/lesson4/Ci.md#compilation-et-exécution-des-tests)  
#### [Publier votre documentation sur gitlab pages](lessons/lesson4/Ci.md#publier-votre-documentation-sur-gitlab-pages)  
#### [Publication d'images docker](lessons/lesson4/Ci.md#publication-dimages-docker)  