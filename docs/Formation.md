# WebApi .NET Core

Du développement au déploiement de votre WebApi .NET Core

## Objectifs

- Maîtriser le fonctionnement de base d’ASP.NET Core et de .NET Core.
- Appréhender la structuration d'un projet
- Comprendre le cycle de vie d'une application du développement à la production

## Public

Développeurs, chefs de projet techniques ayant les bases du développement d’applications avec ASP.NET WebApi ou ASP.NET MVC ou souhaitant obtenir une vision d'ensemble d'un projet WebApi utilisant le framework ASP.NET Core MVC

## Pré-requis

Maîtrise d’un langage .NET (idéalement C#), connaissance d'un framework ASP.NET WebApi ou ASP.NET MVC

## Méthodes pédagogiques

40 % théorie / 60 % pratique

## Description

Le framework ASP.NET Core est l'aboutissement de modifications d'architecture du framework ASP.NET nous proposant un framework plus léger et modulaire, de meilleures performances, une meilleure testabilité ainsi que des possibilités d'hébergement multiples comme par exemple Docker.  
Au cours de cette formation nous allons comprendre le fonctionnement du framework ASP.NET Core MVC et comment l'exploiter dans la conception d'une WebApi afin d'améliorer la qualité de notre application WebApi, en passant par des processus d'intégration et de livraison continue.

## Programme

Introduction

- Différences avec .Net Framework
- Avantages de .Net Core

Actions

- Définition
- Routage
- Format de la réponse

Middleware

- Fonctionnement
- Organisation de notre pipeline

Injection de dépendances

- Principes et cycle de vie
- .Net Core et l'injection de dépendence
- Définition et utilisation d'un middleware

Settings

- Interfaçage avec des fichiers de settings
- Surcharge par environnement

Logging

- Providers intégrés
- Exploitation des logs

Structuration d'une solution

- Layers
- Organisation entre les layers

Documenter son API

- Intérêt
- Mise en place de Swagger

Authentification et Autorisation

- Json Web Token (JWT)
- Cookie d'authentification
- Authentification multiple
- Autorisations

Déploiement et CI

- Docker
- Test unitaire et test d'intégration
- Gitlab CI