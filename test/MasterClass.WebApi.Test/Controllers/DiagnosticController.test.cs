using System.Net;
using System.Net.Http;
using MasterClass.WebApi.Test.Controllers.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.WithWebHostBuilder(builder => builder.UseEnvironment("ci")).CreateClient();
        }

        [Theory]
        [ClassData(typeof(HealthCheckData))]
        public async void HealthCheck_Get_Status200(HttpMethod method)
        {
            var response = await _client.SendAsync(new HttpRequestMessage(method, "api/_system/healthcheck"));
            
            if (!HttpMethods.IsHead(method.Method)
                && !HttpMethods.IsDelete(method.Method)
                && !HttpMethods.IsTrace(method.Method))
            {
                Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
            }

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}