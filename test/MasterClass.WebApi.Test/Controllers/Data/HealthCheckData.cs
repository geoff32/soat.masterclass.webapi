using System.Collections;
using System.Collections.Generic;
using System.Net.Http;

namespace MasterClass.WebApi.Test.Controllers.Data
{
    public class HealthCheckData : IEnumerable<object[]>
    {
        private readonly List<object[]> _data;

        public HealthCheckData()
        {
            _data = new List<object[]>
            {
                new object[] { HttpMethod.Get },
                new object[] { HttpMethod.Head }
            };
        }

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}